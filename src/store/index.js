import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    events: []
  },
  actions: {
    /**
     * get All Events from the server
     */
    async getEventList({ commit }) {
      await axios.get(
        'getall', 
        { headers: {
          'Authorization': 'Bearer 132456798_SAMPLE_TOKEN',
        }}
      )
        .then(response => {
          commit('SET_EV_LIST', response.data)
        })
    },
    /**
     * POST Edit Event and commit the new server data to the state mutation
     */
    async updateEvent( { commit }, editedEventObject ) {
      console.log('Hello from store. This is the payload======', editedEventObject);
      await axios.post(
        'edit',
        editedEventObject, 
       {
         headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer 132456798_SAMPLE_TOKEN`
        }
      })
      .then(
        response => commit('SET_UPDT_EVTS', response.data) 
      )
    },
    async deleteEvent({ commit }, selectedEventObject) {
      console.log('hi store', selectedEventObject);
      await axios.post(
        'delete',
        selectedEventObject, 
       {
         headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer 132456798_SAMPLE_TOKEN`
        }
      })
      .then(
        response => commit('SET_UPDT_EVTS', response.data) 
      )
    }
  },
  mutations: {
    SET_EV_LIST(state, events) {
      state.events = events
      // console.log(state.events);
    },
    SET_UPDT_EVTS(state, events) {
      state.events = events
      let evParsed = JSON.parse(JSON.stringify(state.events.events))
      console.log("Hello from server. Status Code: 200 OK======", evParsed);
    }
  },
  modules: {
  }
})
