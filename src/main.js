import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios';
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

Vue.use(require('vue-moment'));

axios.defaults.baseURL = 'https://challenge.pluralo.com/event';

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
